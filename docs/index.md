---
layout: home
hero:
  name: LuoTian-ACG
  text: 探究技术的意义
  tagline: 心如花木，向阳而生
#  image:
#    src: https://api.likepoems.com/img/bing
#    alt: image
  actions:
    - theme: brand
      text: 开始学习
      link: /study/
features:
- title: 学习
  details: 学习是充满思想的劳动...

- title: 生活
  details: 世上只有一种英雄主义，就是在认清生活真相之后依然热爱生活...

- title: 价值
  details: 只有一种成功——能以你自己的生活方式度过你的一生...
---